## Aggregation API Project

This repository contains a .NET API project for aggregating data from multiple external APIs and accompanying unit tests.
The external APIs used are:

- Open weather map - weather info per city:
  - documentation: `https://openweathermap.org/current`
- Github - repos per user:
  - documentation: `https://docs.github.com/en/rest/repos/repos?apiVersion=2022-11-28#list-repositories-for-a-user`
- Github - users:
  - documentation: `https://docs.github.com/en/rest/users/users?apiVersion=2022-11-28#list-users`

The APIs are not all related to each other. In this case, the API aggregation is useful for handling separate
requests to external APIs in parallel, reducing network overload and speeding up auth processes, as the consumer
of our aggregated API only has to get authorized once.

## Prerequisites

- .NET SDK (version 8.0)
- A code editor like Visual Studio
- Setting up Open weather API key to make requests against their endpoints.
  Get one here: `https://home.openweathermap.org/api_keys`

## Setup

Clone the repository. From terminal:

`git clone https://gitlab.com/your-username/your-repository.git`

Path to the project solution: aggregation-api-project/App-Project/ApiAggregation.sln
Open the project solution, for example in Visual Studio.
Restore to get the packages and build the solution.

## Running the API

To run the application, run the solution on Visual Sturio.
Otherwise, from vscode navigate to the project directory and use the dotnet run command:

`cd AppProject`
`dotnet run`

Use Postman to test the API.

- First, get a valid token to authorize against the aggregated API.
  - Do that by executing a POST request against: `http://localhost:5067/authorization-api/Authorization/login`
  - Copy the token.
- Create a GET request against `http://localhost:5067/api/Aggregation`.
  - On Authorization tab, select Bearer token Auth Type and set the token from previous step.
  - Parameters:
    - `weather_map_city` (required):
      - string
      - city name for which the weather to get
      - related to open weather map API
    - `github_repos_user` (required):
      - string
      - user name for which the repos to get
      - related to github repos per user API
    - `github_repos_limit` (optional):
      - int, defaulted to 30
      - max records to return per request
      - related to github repos per user API
    - `github_repos_sort_direction` (optional):
      - string, defaulted to `asc`
      - available values: `asc`, `desc`
      - direction of sorting results
      - related to github repos per user API
    - `github_repos_sort` (optional):
      - string
      - string, defaulted to `full_name`
      - available values: `created`, `updated`, `pushed`, `full_name`
      - direction of sorting results
      - related to github repos per user API
    - `github_users_limit` (optional):
      - int, defaulted to 30
      - max records to return per request
      - related to github users API, optional
- Regarding the aggregated API's response:

  - It contains the response from each API in the following format:
    - {
      "apiName": "the-api-code-name",
      "isSuccess": "true-or-false",
      "data": "response-in-case-of-success",
      "error": "error-in-case-of-failure"
      }
    - Naming conventions are not proper - must have used properties to properly tranform
      them to JSON namings, like api_name instead of apiName. Didn't have time to fix this.
  - The final response from the aggregated API will look like:
    - "weather": {
      "apiName": "OpenWeatherMapForCity",
      "isSuccess": "true-or-false",
      "data": "response-in-case-of-success",
      "error": "error-in-case-of-failure"
      },
      "githubRepositories": {
      "apiName": "GithubRepositoriesForUser",
      "isSuccess": "true-or-false",
      "data": "response-in-case-of-success",
      "error": "error-in-case-of-failure"
      },
      "githubUsers": {
      "apiName": "GithubUsers",
      "isSuccess": "true-or-false",
      "data": "response-in-case-of-success",
      "error": "error-in-case-of-failure"
      }

The parameters used are some of the parameters used in the external APIs. There is no extra logic in the API
aggrefation application, the parameters are just applied to the external APIs. Read more for the parameters
on the external APIs documentation in the links provided at the top.

## Running the Tests

To run the tests, better use Visual Studio. By opening the solution, the test will apear
in the Test explorer.

## Technical details - decisions

### API response models

Only some of the response data are added to each of the models. Reason: simplicity and can be adjusted based on needs.
C# naming conventions not followed on Model files to match the JSON names and get mapped properly. Didn't have the time
to fix it properly by adding the right properties.
Also, on `Result` model used in each external API response, the naming is not fixed to be displayed in JSON style when
used in JSON format. Again, will have to use the right properties to fix this.

### Filtering and sorting of aggregated data

Implemented basic filtering (based on response size) and sorting due to time limitations.
All the filtering and sorting parameters are provided as query parameters.
For more complex cases of filtering and sorting, adjustments are needed, for example,
a POST request instead of GET might be needed to pass the parameters as JSON body.

### Authorization

JWT Bearer authorization is used. A valid token is generated from the `http://localhost:5067/authorization-api/Authorization/login`,
for testing purposes, simulating an auth server like Microsoft Entra ID. The aggregation API requires a valid
JWT Bearer token from the valid issuer and audience that has not expired to proceed with the request.

### Logging

Logging is configured using the ILogger interface.

### Error Handling

Global error handling is implemented using middleware. The `ExceptionHandlingMiddleware` class handles exceptions
and returns appropriate HTTP status codes.
Error handling logic has been implemented for each external API:

- If any of them fails, then we log it's error on the response but the rest of the executions are not interrupted.
- Retry policy has been added to each of the external API services.

Improvements:

- Add retry policy on global level.

### Cache

Due to time limitations, caching is not implemented.
Describing what I would do though:

- Cache service instances
- OpenWeatherMap: Cache weather data results based on the city
  - Weather is not drastically changed every minute. Keep cached data for like 3 hours before refreshing cache.
- GithubUsers:
  - One idea is to retrieve all users from github and store them in memory or better a local DB as the result might be huge.
    Then query this storage every time users are requested. Refresh this storage with new data every X time.
    Considerations based on the usage workload of this API.
- GithubRepos: similar thoughts with above.

### Testing

Some unit tests are implemented using the xUnit framework and Moq for mocking dependencies.
They are just sample tests for the `OpenWeatherMapService`, in the sense that only few sccenarios are tested.

Improvements:

- Unit testing for all separate services.
  - Add scenarios for non transient exception -> retry policy not enabled.
- Integration tests that test the aggregation API endpoint
  - Mock consumer gets authorized
  - Proper global error handling in case of application error.
  - Proper results in case one of the external APIs fails -> the rest are not interrupted.
