// Models to interact with https://api.openweathermap.org/data/2.5/weather?q={city}&appid={apiKey}.
// The are defined based on the API's dumentation: 
// https://openweathermap.org/current

using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Representing the OpenWeatherMap API response.
/// Not all response atttributes included. Adjust based on needs.
/// </summary>
public class WeatherData
{
    [JsonPropertyName("coord")]
    public Coord Coord { get; set; } = new Coord();

    [JsonPropertyName("weather")]
    public Weather[] Weather { get; set; } = Array.Empty<Weather>();

    [JsonPropertyName("main")]
    public Main Main { get; set; } = new Main();

    [JsonPropertyName("visibility")]
    public int Visibility { get; set; }

    [JsonPropertyName("dt")]
    public int Dt { get; set; }

    [JsonPropertyName("timezone")]
    public int Timezone { get; set; }

    [JsonPropertyName("name")]
    public string? Name { get; set; }
}

public class Coord
{
    [JsonPropertyName("lon")]
    public double Lon { get; set; }

    [JsonPropertyName("lat")]
    public double Lat { get; set; }
}

public class Weather
{
    [JsonPropertyName("main")]
    public string? Main { get; set; }

    [JsonPropertyName("description")]
    public string? Description { get; set; }
}

public class Main
{
    [JsonPropertyName("temp")]
    public double Temp { get; set; }

    [JsonPropertyName("feels_like")]
    public double FeelsLike { get; set; }

    [JsonPropertyName("temp_min")]
    public double TempMin { get; set; }

    [JsonPropertyName("temp_max")]
    public double TempMax { get; set; }

    [JsonPropertyName("pressure")]
    public int Pressure { get; set; }

    [JsonPropertyName("humidity")]
    public int Humidity { get; set; }

    [JsonPropertyName("sea_level")]
    public int SeaLevel { get; set; }

    [JsonPropertyName("grnd_level")]
    public int GrndLevel { get; set; }
}

/// <summary>
/// Represents the parameters regarding OpenWeatherMap API.
/// Only some of the available parameters are included. More can get added based on needs.
/// The class members are defaulted based on the defaults from the API's documentation.
/// Improvements: 
///  - Validate the values of each field are valid, based on APIs documentation.
/// </summary>
public class OpenWeatherMapParameters
{
    // The city for which to search the weather.
    [FromQuery(Name = "weather_map_city")]
    public required string WeatherMapCity { get; set; }
}