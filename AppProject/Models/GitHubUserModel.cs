
/// Models to interact with https://api.github.com/users.
/// The are defined based on the API's dumentation: 
/// https://docs.github.com/en/rest/users/users?apiVersion=2022-11-28#list-users

using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Representing the OpenWeatherMap API response.
/// Not all response atttributes included. Adjust based on needs.
/// </summary>
public class GithubUserData
{
    [JsonPropertyName("name")]
    public string? Name { get; set; }

    [JsonPropertyName("email")]
    public string? Email { get; set; }

    [JsonPropertyName("login")]
    public string? Login { get; set; }

    [JsonPropertyName("url")]
    public string? Url { get; set; }

    [JsonPropertyName("html_url")]
    public string? HtmlUrl { get; set; }

    [JsonPropertyName("repos_url")]
    public string? ReposUrl { get; set; }

    [JsonPropertyName("type")]
    public string? Type { get; set; }
}

/// <summary>
/// Represents the parameters regarding SpotifyPlaylists API.
/// Only some of the available parameters are included. More can get added based on needs.
/// The class members are defaulted based on the defaults from the API's documentation.
/// Improvements: 
///  - Validate the values of each field are valid, based on APIs documentation.
/// </summary>
public class GithubUsersParameters
{
    // The max number of results.
    [FromQuery(Name = "github_users_limit")]
    public int GithubUsersLimit { get; set; } = 30;
}