/// <summary>
/// Defines the model of the aggregated result from the different external APIs.
/// </summary>
public class AggregatedData
{
    public required Result<WeatherData> Weather { get; set; }
    public required Result<GithubRepositoryData[]> GithubRepositories { get; set; }
    public required Result<GithubUserData[]> GithubUsers { get; set; }
}