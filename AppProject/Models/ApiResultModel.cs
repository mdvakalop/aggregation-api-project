

using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

/// <summary>
/// Used to provide suvvess / error information and results for each API.
/// </summary>
public class Result<T>
{
    // Determines the API name which results are included.
    [JsonPropertyName("api_name")]
    public string ApiName { get; }

    // Determines if the request to the API succedded.
    [JsonPropertyName("is_success")]
    public bool IsSuccess { get; }

    // The response data in case of a successful request.
    [JsonPropertyName("data")]
    public T? Data { get; }

    // The error message in case of an unsuccessful request.
    [JsonPropertyName("error")]
    public string? Error { get; }

    protected Result(string apiName, T? value, bool isSuccess, string? error)
    {
        // Validate input
        if ((isSuccess && (value is null || error is not null))
            || (!isSuccess && (value is not null || error is null)))
        {
            throw new ValidationException($"Invalid response from {apiName} API.");
        }

        ApiName = apiName;
        Data = value;
        IsSuccess = isSuccess;
        Error = error;
    }

    public static Result<T> Success(string apiName, T value) => new Result<T>(apiName, value, true, null);
    public static Result<T> Failure(string apiName, string error) => new Result<T>(apiName, default, false, error);
}