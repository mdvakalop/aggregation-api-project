// Models to interact with https://api.github.com/users/{user}/repos.
// The are defined based on the API's dumentation: 
// https://docs.github.com/en/rest/repos/repos?apiVersion=2022-11-28#list-repositories-for-a-user

using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;

/// <summary>
/// Representing the GithubRepositories API response.
/// Not all response atttributes included. Adjust based on needs.
/// </summary>
public class GithubRepositoryData
{
    [JsonPropertyName("name")]
    public string? Name { get; set; }

    [JsonPropertyName("owner")]
    public SimpleUser Owner { get; set; } = new SimpleUser();

    [JsonPropertyName("private")]
    public bool Private { get; set; }

    [JsonPropertyName("html_url")]
    public string? HtmlUrl { get; set; }

    [JsonPropertyName("description")]
    public string? Description { get; set; }

    [JsonPropertyName("url")]
    public string? Url { get; set; }

    [JsonPropertyName("homepage")]
    public string? Homepage { get; set; }

    [JsonPropertyName("language")]
    public string? Language { get; set; }

    [JsonPropertyName("visibility")]
    public string? Visibility { get; set; }

    [JsonPropertyName("created_at")]
    public DateTime? CreatedAt { get; set; }

    [JsonPropertyName("updated_at")]
    public DateTime? UpdatedAt { get; set; }
}

public class SimpleUser
{
    [JsonPropertyName("name")]
    public string? Name { get; set; }

    [JsonPropertyName("email")]
    public string? Email { get; set; }

    [JsonPropertyName("login")]
    public string? Login { get; set; }
}


/// <summary>
/// Represents the parameters regarding GithubRepositories API.
/// Only some of the available parameters are included. More can get added based on needs.
/// The class members are defaulted based on the defaults from the API's documentation.
/// Improvements: 
///  - Validate the values of each field are valid, based on APIs documentation.
/// </summary>
public class GithubRepositoriesParameters
{
    // The user name for which the repositories to search.
    [FromQuery(Name = "github_repos_user")]
    public required string GithubReposUser { get; set; }

    // The value based on which to sort the results.
    [FromQuery(Name = "github_repos_sort")]
    public string GithubReposSort { get; set; } = "full_name";

    // The direction based on which to sort the results.
    [FromQuery(Name = "github_repos_sort_direction")]
    public string GithubReposSortDirection { get; set; } = "asc";

    // The max number of results.
    [FromQuery(Name = "github_repos_limit")]
    public int GithubReposLimit { get; set; } = 30;
}