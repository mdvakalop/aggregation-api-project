using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("aggregation-api/[controller]")]
public class AggregationController : ControllerBase
{
    private readonly AggregationService _aggregationService;

    public AggregationController(AggregationService aggregationService)
    {
        _aggregationService = aggregationService;
    }

    [HttpGet]
    [Authorize] // Requires a valid JWT
    public async Task<IActionResult> GetAggregatedData(
        [FromQuery] OpenWeatherMapParameters weatherParameters,
        [FromQuery] GithubRepositoriesParameters githubRepositoriesParameters,
        [FromQuery] GithubUsersParameters githubUsersParameters)
    {
        var data = await _aggregationService.GetAggregatedDataAsync(
            weatherParameters, githubRepositoriesParameters, githubUsersParameters);

        return Ok(data);
    }
}
