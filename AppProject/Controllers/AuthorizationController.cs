using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

/// <summary>
/// This controller simulates an authorization system, like Microsoft Entra ID, that
/// would be used to provide a valid token based on the user credentials.  
/// </summary>
[ApiController]
[Route("authorization-api/[controller]")]
public class AuthorizationController : ControllerBase
{
    [HttpPost("login")]
    public IActionResult Login()
    {
        var issuer = ValidDomains.Issuer;
        var audience = ValidDomains.Audience;
        var key = Encoding.ASCII.GetBytes(Secrets.JwtSecretKey);

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity([
                new Claim("Id", Guid.NewGuid().ToString())]),
            Expires = DateTime.UtcNow.AddMinutes(5),
            Issuer = issuer,
            Audience = audience,
            SigningCredentials = new SigningCredentials(
                new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha512Signature)
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        var stringToken = tokenHandler.WriteToken(token);

        return Ok(new { Token = stringToken });
    }
}
