public static class HttpClientConfig
{
    public static void ConfigureGithubClient(HttpClient client)
    {
        client.BaseAddress = new Uri(ApiBaseUris.GitHubUri);
        client.DefaultRequestHeaders.Add("User-Agent", "ApiAggregationService");
    }

    public static void ConfigureOpenWeatherMapClient(HttpClient client)
    {
        client.BaseAddress = new Uri(ApiBaseUris.OpenWeatherMapUri);
    }
}