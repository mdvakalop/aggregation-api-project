using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text.Json;

/// <summary>
/// Middleware injected in the application to handle exceptions from the system.
/// Note that unsuccessful requests to the external APIs will be handled separately
/// to avoid breaking the execution of other services.
/// </summary>
public class ExceptionHandlingMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<ExceptionHandlingMiddleware> _logger;

    public ExceptionHandlingMiddleware(RequestDelegate next, ILogger<ExceptionHandlingMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (Exception ex)
        {
            _logger.LogError($"Something went wrong!!!");
            await HandleExceptionAsync(httpContext, ex);
        }
    }

    /// <summary>
    /// Handles the exception by returning a proper Http error code based on the exception type.
    /// </summary>
    private static Task HandleExceptionAsync(HttpContext context, Exception exception)
    {
        context.Response.ContentType = "application/json";
        context.Response.StatusCode = GetHttpExceptionStatusCode(exception);

        var result = JsonSerializer.Serialize(new { error = exception.Message });
        return context.Response.WriteAsync(result);
    }

    /// <summary>
    /// Returns the related http error code based on the exception occured.
    /// Improvements:
    ///  - Handle more exception types, as the application grows.
    /// </summary>
    private static int GetHttpExceptionStatusCode(Exception exception)
    {
        switch (exception)
        {
            case ArgumentNullException:
            case ArgumentException:
            case ValidationException:
                return (int)HttpStatusCode.BadRequest;
            case HttpRequestException httpRequestException:
                return httpRequestException.StatusCode.HasValue
                    ? (int)httpRequestException.StatusCode.Value
                    : (int)HttpStatusCode.ServiceUnavailable;
            case TimeoutException:
                return (int)HttpStatusCode.GatewayTimeout;
            case UnauthorizedAccessException:
                return (int)HttpStatusCode.Unauthorized;

        }

        return (int)HttpStatusCode.InternalServerError;

    }
}