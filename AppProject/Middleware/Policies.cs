using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;


public static class Policies
{
    /// <summary>
    /// Retry policy used on each external API to retry in case of a transient error.
    /// Improvement:
    ///  - Use retry policies on the aggregated service as well, in case it fails due
    //    to internal transient issues.
    /// </summary>
    public static IAsyncPolicy<HttpResponseMessage> RetryPolicy(ILogger logger)
    {
        return HttpPolicyExtensions
            .HandleTransientHttpError()
            .Or<TimeoutRejectedException>()
            .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.InternalServerError) // Ensure we handle 500 status code
            .WaitAndRetryAsync(
                3,
                retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                onRetry: (outcome, timespan, retryAttempt, context) =>
                {
                    logger.LogWarning($"Retry #{retryAttempt} : Reason: {outcome.Exception.Message}");
                });
    }
}

