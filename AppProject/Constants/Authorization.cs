// Constant values required for the authorization logic.
// Normally, those constants should not be hardocoded but be stored in azure KeyVault
// or equivalent instead. For now, they get set here for testing purposes.

public class Secrets
{
    public const string JwtSecretKey =
        "Secret key for testing - normally it should be stored safely, like in azure key vault.";

}

public static class ApiKeys
{
    public const string OpenWeatherMapApiKey = "Replace with your open weather map api key";

}

// The members of this class are set like that for testing purposes. When the application
// is deployed in production, the values will have to be corrected as described for each of them.
public static class ValidDomains
{
    // Replace with the URL of the authentication server.
    public const string Issuer = "https://a-valid-issuer.com";

    // Replace with the URL of the aggregation-api application.
    public const string Audience = "https://a-valid-audience.com";
}