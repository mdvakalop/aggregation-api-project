public static class ApiBaseUris
{
    public static readonly string GitHubUri = "https://api.github.com/";
    public static readonly string OpenWeatherMapUri = "https://api.openweathermap.org/data/2.5/";
}