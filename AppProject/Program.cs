using Microsoft.AspNetCore.Authentication.JwtBearer;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(JwtBearerConfig.ConfigureOptions);
builder.Services.AddAuthorization();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddLogging();

// Register services
builder.Services.AddScoped<AggregationService>();

// Register custom HttpClient services
builder.Services.AddHttpClient<OpenWeatherMapService>(HttpClientConfig.ConfigureOpenWeatherMapClient);
builder.Services.AddHttpClient<GithubRepositoriesService>(HttpClientConfig.ConfigureGithubClient);
builder.Services.AddHttpClient<GithubUsersService>(HttpClientConfig.ConfigureGithubClient);

var app = builder.Build();

app.UseMiddleware<ExceptionHandlingMiddleware>();

// Add authentication and authorization middleware
app.UseAuthentication();
app.UseAuthorization();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.MapControllers();

app.Run();