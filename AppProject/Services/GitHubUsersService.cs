public class GithubUsersService : BaseService<GithubUserData[]>
{
    public GithubUsersService(HttpClient httpClient, ILogger<GithubUsersService> logger)
        : base("GithubUsers", httpClient, logger) { }

    protected override string BuildUrl(object parameters)
    {
        if (parameters is not GithubUsersParameters)
        {
            throw new ArgumentException(
                "Invalid parameters passed to Github users API.");
        }

        var spotifyParameters = (GithubUsersParameters)parameters;

        return $"users?per_page={spotifyParameters.GithubUsersLimit}";
    }
}