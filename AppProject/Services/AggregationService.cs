public class AggregationService
{
    private readonly OpenWeatherMapService _weatherService;
    private readonly GithubRepositoriesService _githubRepositoriesService;
    private readonly GithubUsersService _githubUsersService;

    public AggregationService(
        OpenWeatherMapService weatherService,
        GithubRepositoriesService githubRepositoriesService,
        GithubUsersService githubUsersService)
    {
        _weatherService = weatherService;
        _githubRepositoriesService = githubRepositoriesService;
        _githubUsersService = githubUsersService;
    }

    public async Task<AggregatedData> GetAggregatedDataAsync(
        OpenWeatherMapParameters weatherParameters,
        GithubRepositoriesParameters githubRepositoriesParameters,
        GithubUsersParameters githubUsersParameters)
    {
        var weatherTask = _weatherService.GetDataAsync(weatherParameters);
        var githubRepositoriesTask = _githubRepositoriesService.GetDataAsync(githubRepositoriesParameters);
        var githubUsersTask = _githubUsersService.GetDataAsync(githubUsersParameters);

        await Task.WhenAll(weatherTask, githubRepositoriesTask, githubUsersTask);

        return new AggregatedData
        {
            Weather = weatherTask.Result,
            GithubRepositories = githubRepositoriesTask.Result,
            GithubUsers = githubUsersTask.Result
        };
    }
}
