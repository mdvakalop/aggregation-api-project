public class GithubRepositoriesService : BaseService<GithubRepositoryData[]>
{
    public GithubRepositoriesService(HttpClient httpClient, ILogger<GithubRepositoriesService> logger)
        : base("GithubRepositoriesForUser", httpClient, logger) { }

    protected override string BuildUrl(object parameters)
    {
        if (parameters is not GithubRepositoriesParameters)
        {
            throw new ArgumentException(
                "Invalid parameters passed to Github repositories API.");
        }

        var githubParameters = (GithubRepositoriesParameters)parameters;

        return $"users/{githubParameters.GithubReposUser}/repos" +
            $"?sort={githubParameters.GithubReposSort}" +
            $"&direction={githubParameters.GithubReposSortDirection}" +
            $"&per_page={githubParameters.GithubReposLimit}";
    }
}