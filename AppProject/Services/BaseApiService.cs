/// <summary>
/// This class is the base class for all services that get data from an external API.
/// The generic type T is the data model that describes the response model of the
/// external API.
/// </summary>
public abstract class BaseService<T>
{
    protected readonly string _apiName;
    protected readonly HttpClient _httpClient;
    protected readonly ILogger<BaseService<T>> _logger;

    protected BaseService(string apiName, HttpClient httpClient, ILogger<BaseService<T>> logger)
    {
        _apiName = apiName;
        _httpClient = httpClient;
        _logger = logger;
    }

    /// <summary>
    /// Builds the URL that will be used to request from the external API.
    /// Improvement:
    ///  - Instead of accepting parameters parameter with type object, define
    ///   an interface for valid parameters or make the method generic.
    /// </summary>
    protected abstract string BuildUrl(object parameters);

    /// <summary>
    /// Executes the request against the external API and ensures success.
    /// Returns the HTTP response from the external API.
    /// </summary>
    public async Task<HttpResponseMessage> GetResponseAsync(object parameters)
    {
        var url = BuildUrl(parameters);
        var response = await _httpClient.GetAsync(url);

        response.EnsureSuccessStatusCode();

        return response;
    }

    /// <summary>
    /// Gets the request result based on the data model type T from the external API.
    /// The request is executed inside a retry policy in case the external API is
    /// unavailable or has transient issues.
    /// Returns the result data based on the data model T.
    /// </summary>
    public async Task<Result<T>> GetDataAsync(object parameters)
    {
        try
        {
            var response = await Policies.RetryPolicy(_logger).ExecuteAsync(() => GetResponseAsync(parameters));
            var data = await response.Content.ReadAsAsync<T>();

            return Result<T>.Success(_apiName, data);

        }
        catch (HttpRequestException ex)
        {
            var statusCode = ex.StatusCode is not null
                ? ((int)ex.StatusCode).ToString()
                : "undefined status code";

            return Result<T>.Failure(_apiName, $"{ex.GetType().Name} - status code {statusCode}: {ex.Message}");
        }
        catch (Exception ex)
        {
            return Result<T>.Failure(_apiName, $"{ex.GetType().Name}: {ex.Message}");
        }
    }
}

