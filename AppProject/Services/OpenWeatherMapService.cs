public class OpenWeatherMapService : BaseService<WeatherData>
{
    public OpenWeatherMapService(HttpClient httpClient, ILogger<OpenWeatherMapService> logger)
        : base("OpenWeatherMapForCity", httpClient, logger) { }

    protected override string BuildUrl(object parameters)
    {
        if (parameters is not OpenWeatherMapParameters)
        {
            throw new ArgumentException(
                "Invalid parameters passed to OpenWeatherMap repositories API.");
        }

        var weatherParameters = (OpenWeatherMapParameters)parameters;

        return $"weather?q={weatherParameters.WeatherMapCity}&appid={ApiKeys.OpenWeatherMapApiKey}";
    }
}

