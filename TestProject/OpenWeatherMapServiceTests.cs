using Microsoft.Extensions.Logging;
using System.Net;
using Moq;
using Moq.Protected;
using System.Net.NetworkInformation;

namespace TestProject
{
    public class OpenWeatherMapServiceTests
    {
        private readonly Mock<HttpMessageHandler> _httpMessageHandlerMock;
        private readonly HttpClient _httpClient;
        private readonly Mock<ILogger<OpenWeatherMapService>> _loggerMock;
        private readonly OpenWeatherMapService _service;

        private const string TestCityName = "London";
        private const double TestCityLon = -0.12;
        private const double TestCityLat = 51.51;

        public OpenWeatherMapServiceTests()
        {
            _httpMessageHandlerMock = new Mock<HttpMessageHandler>();
            _httpClient = new HttpClient(_httpMessageHandlerMock.Object)
            {
                BaseAddress = new Uri(ApiBaseUris.OpenWeatherMapUri)
            };
            _loggerMock = new Mock<ILogger<OpenWeatherMapService>>();

            _service = new OpenWeatherMapService(_httpClient, _loggerMock.Object);
        }

        [Fact]
        public async Task GetWeatherDataAsync_ReturnsSuccess_WhenApiCallIsSuccessful()
        {
            // Arrange
            var parameters = new OpenWeatherMapParameters { weather_map_city = TestCityName };
            var expectedResult = new WeatherData { 
                coord = new Coord { lon = TestCityLon, lat = TestCityLat } };
            var responseMessage = GetOkWeatherDataResponseMessage(expectedResult);

            _httpMessageHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(responseMessage);

            // Act
            var result = await _service.GetDataAsync(parameters);

            // Assert
            Assert.True(result.IsSuccess);
            // Improvement: create comparison method that will compare all
            // WeatherData members of 2 class instances.
            Assert.Equal(expectedResult.coord.lon, result.Data?.coord.lon);
            Assert.Equal(expectedResult.coord.lat, result.Data?.coord.lat);
        }

        /// Note: Expected to take longer as the retry policy waits and reatries.
        [Fact]
        public async Task GetWeatherDataAsync_ReturnsFailure_WhenApiCallFails()
        {
            // Arrange
            var parameters = new OpenWeatherMapParameters { weather_map_city = TestCityName };
            var responseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError);

            _httpMessageHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(responseMessage);

            // Act
            var result = await _service.GetDataAsync(parameters);

            // Assert
            Assert.False(result.IsSuccess);
            Assert.Contains($"status code {(int)HttpStatusCode.InternalServerError}", result.Error);
        }

        [Fact]
        public async Task GetWeatherDataAsync_RetriesOnTransientError()
        {
            // Arrange
            var parameters = new OpenWeatherMapParameters { weather_map_city = TestCityName };
            var responseMessage = GetOkWeatherDataResponseMessage(new WeatherData());
            const string ExceptionMessage = "Transient error";

            _httpMessageHandlerMock.Protected()
                .SetupSequence<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Throws(GetTransientHttpRequestException(ExceptionMessage))
                .Throws(GetTransientHttpRequestException(ExceptionMessage))
                .ReturnsAsync(responseMessage);

            // Act
            var result = await _service.GetDataAsync(parameters);

            // Assert
            Assert.True(result.IsSuccess);
            AssertLoggerRetriesAndWarningMessage(2, ExceptionMessage);
        }

        private void AssertLoggerRetriesAndWarningMessage(
            int expectedRetries, string warningMessage = "")
        {
            _loggerMock.Verify(
                x => x.Log(
                    LogLevel.Warning,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => v.ToString().Contains(warningMessage)),
                    It.IsAny<Exception>(),
                    It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)),
                Times.AtLeast(expectedRetries));
        }

        private HttpResponseMessage GetOkWeatherDataResponseMessage(WeatherData weatherData)
        {
            var responseMessage = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(System.Text.Json.JsonSerializer.Serialize(weatherData))
            };
            responseMessage.Content.Headers.ContentType =
                new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            return responseMessage;
        }

        private HttpRequestException GetTransientHttpRequestException(
            string exceptionMessage = "Transient error")
        {
            return new HttpRequestException
                (exceptionMessage, null, HttpStatusCode.RequestTimeout);
        }
    }
}